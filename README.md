# Client web du projet Letsgo Lego

## Vue d'ensemble

Letsgo Lego est un site de vente aux enchères de Lego d'occasion et de collection, destiné aux particuliers (C2C). Nous proposons différents types d'enchères en fonction du produit vendu, afin de répondre aux besoins et aux préférences de nos utilisateurs.

## Pour commencer

-   `git clone https://gricad-gitlab.univ-grenoble-alpes.fr/iut2-info-stud/2022-s3/s3.01/team-08/frontend.git`
-   `cd frontend`
-   `npm install`
-   `npm run dev`

### Versions

-   Node.js : v18.9.0
-   Npm : v8.19.1
-   React : v18.2
    -   React-router-dom : v6.6.1
    -   Styled-components : v5.3.6
    -   HeroIcons : v2.0.13
