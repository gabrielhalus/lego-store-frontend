import styled from 'styled-components'

export const Container = styled.div``

export const Section = styled.div`
    padding-block: 32px 24px;

    display: flex;
    flex-direction: column;
    row-gap: 24px;
`

export const HGroup = styled.div`
    display: flex;
    flex-direction: column;
    row-gap: 8px;

    padding-inline: 24px;
`

export const SectionTitle = styled.h2`
    font-weight: 400;
    font-size: 20px;
    color: var(--text-base-primary);
`

export const SectionSubTitle = styled.h3`
    font-weight: 400;
    font-size: 16px;
    color: var(--text-base-secondary);
`
