import React, { useContext } from 'react'
import Banner from '../../components/Banner'
import Navbar from '../../components/Navbar'
import Footer from '../../components/Footer'
import { Container, HGroup, Section, SectionSubTitle, SectionTitle } from './HomeElements'
import { useSelector } from 'react-redux'
import { UidContext } from '../../components/AppContext'

const Home = () => {
    const uid = useContext(UidContext)
    const userData = useSelector(state => state.user)

    return (
        <Container>
            <Navbar />
            {uid && (
                <Banner>
                    Bienvenue{' '}
                    <b>
                        {userData.name} {userData.surname}
                    </b>
                </Banner>
            )}
            <Section>
                <HGroup>
                    <SectionTitle>Elles pourraient vous intéresser</SectionTitle>
                    <SectionSubTitle>Ventes les plus populaires</SectionSubTitle>
                </HGroup>
            </Section>
            <Section>
                <HGroup>
                    <SectionTitle>La fin est proche</SectionTitle>
                    <SectionSubTitle>Ventes qui arrivent à leur fin</SectionSubTitle>
                </HGroup>
            </Section>
            <Footer />
        </Container>
    )
}

export default Home
