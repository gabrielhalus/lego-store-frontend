import React, { useContext } from 'react'
import { UidContext } from '../../components/AppContext'
import Log from '../../components/Log'
import UpdateProfil from '../../components/Profil/UpdateProfil'

const Profil = () => {
    const uid = useContext(UidContext)

    return <>{uid ? <UpdateProfil /> : <Log signin={true} signup={false} />}</>
}

export default Profil
