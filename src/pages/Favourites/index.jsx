import React from 'react'
import Footer from '../../components/Footer'
import Navbar from '../../components/Navbar'
import { Container } from './FavouritesElements'

function Favourites() {
    return (
        <>
            <Navbar />
            <Container>Favourites</Container>
            <Footer />
        </>
    )
}

export default Favourites
