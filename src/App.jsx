import axios from 'axios'
import { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { UidContext } from './components/AppContext'
import { getUser } from './redux/actions/user.action'
import Routes from './Routes'
import GlobalStyle from './theme/globalStyle'

function App() {
    const [uid, setUid] = useState(null)
    const dispatch = useDispatch()

    useEffect(() => {
        const fetchToken = () => {
            axios({
                method: 'get',
                url: `${import.meta.env.VITE_API_URL}jwtid`,
                withCredentials: true,
            }).then(res => setUid(res.data))
        }
        fetchToken()

        if (uid) dispatch(getUser(uid))
    }, [uid, dispatch])

    return (
        <UidContext.Provider value={uid}>
            <GlobalStyle />
            <Routes />
        </UidContext.Provider>
    )
}

export default App
