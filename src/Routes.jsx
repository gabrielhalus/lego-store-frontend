import React from 'react'
import { BrowserRouter as Router, Routes as Switch, Route } from 'react-router-dom'
import Home from './pages/Home'
import Profil from './pages/Profil'
import Favourites from './pages/Favourites'

const Routes = () => {
    return (
        <Router>
            <Switch>
                <Route path='/' element={<Home />} />
                <Route path='/profil' element={<Profil />} />
                <Route path='/favoris' element={<Favourites />} />
            </Switch>
        </Router>
    )
}

export default Routes
