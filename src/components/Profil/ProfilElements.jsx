import styled from 'styled-components'

export const Container = styled.div`
    padding-block: 24px 48px;
    display: flex;
    flex-direction: column;
    row-gap: 24px;

    @media screen and (min-width: 765px) {
        width: 80%;
        margin: 24px auto 48px;

        border: 1px solid var(--background-primary-disabled);
    }
`

export const BackButton = styled.button`
    background: none;
    border: none;
    cursor: pointer;

    margin-left: 24px;
    display: inline-flex;
    align-items: center;
    column-gap: 8px;

    color: var(--text-base-primary);
    font-weight: 700;
    font-size: 16px;
`

export const NavContainer = styled.div`
    display: flex;
    justify-content: center;
`

export const Nav = styled.nav`
    overflow-x: scroll;
    white-space: nowrap;

    padding: 0 24px;
    display: inline-flex;
    justify-content: flex-start;
    gap: 24px;

    scrollbar-width: none; // for Firefox

    &::-webkit-scrollbar {
        display: none; // for Chrome, Safari, and Opera
    }

    @media screen and (min-width: 765px) {
        gap: 32px;
    }
`

export const ProfilNavLink = styled.button`
    background: none;
    border: none;
    cursor: pointer;

    color: var(--text-base-primary);
    font-size: 16px;
    padding-block: 12px;
    white-space: nowrap;
    &.active {
        border-bottom: 1px solid var(--text-base-primary);
    }
`

export const ProfilNavButton = styled(ProfilNavLink)`
    &:hover {
        color: var(--accent);
    }
`

export const Title = styled.h1`
    align-self: center;
    font-family: 'Playfair Display';
    font-size: 24px;
    font-weight: 700;
    letter-spacing: -3%;
    line-height: 130%;
`
