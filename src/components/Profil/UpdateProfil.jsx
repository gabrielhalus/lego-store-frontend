import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import Navbar from '../Navbar'
import Footer from '../Footer'
import axios from 'axios'
import cookie from 'js-cookie'
import { BackButton, Container, Nav, NavContainer, ProfilNavButton, ProfilNavLink, Title } from './ProfilElements'
import Icon from '../Icon'
import { useNavigate } from 'react-router-dom'

const UpdateProfil = () => {
    const userData = useSelector(state => state.user)
    const navigate = useNavigate()

    const [tab, setTab] = useState('PROFIL')

    const logout = () => {
        console.log(`${import.meta.env.VITE_API_URL}api/user/logout`)
        axios({
            method: 'get',
            url: `${import.meta.env.VITE_API_URL}api/user/logout`,
            withCredentials: true,
        })
            .then(() => {
                removeCookies('jwt')
                window.location = '/'
            })
            .catch(err => console.log(err))
    }

    const removeCookies = key => {
        if (window !== 'undefined') cookie.remove(key, { expires: 1 })
    }

    return (
        <>
            <Navbar />
            <Container>
                <BackButton onClick={() => navigate(-1)}>
                    <Icon name='ArrowLeft' width='20' height='20' />
                    Retour
                </BackButton>
                <NavContainer>
                    <Nav>
                        <ProfilNavLink className={tab === 'PROFIL' && 'active'} onClick={() => setTab('PROFIL')}>
                            Mon profil
                        </ProfilNavLink>
                        <ProfilNavLink className={tab === 'BIDS' && 'active'} onClick={() => setTab('BIDS')}>
                            Mes enchères
                        </ProfilNavLink>
                        <ProfilNavLink className={tab === 'AUCTIONS' && 'active'} onClick={() => setTab('AUCTIONS')}>
                            Mes ventes
                        </ProfilNavLink>
                        <ProfilNavButton onClick={logout}>Deconnexion</ProfilNavButton>
                    </Nav>
                </NavContainer>
                {tab === 'PROFIL' && (
                    <>
                        <Title>Mon profil</Title>
                    </>
                )}
                {tab === 'BIDS' && <Title>Mes enchères</Title>}
                {tab === 'AUCTIONS' && <Title>Mes ventes</Title>}
            </Container>
            <Footer />
        </>
    )
}

export default UpdateProfil
