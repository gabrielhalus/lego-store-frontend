import React, { useEffect, useState } from 'react'
import { NavLink, useLocation } from 'react-router-dom'
import Icon from '../Icon'
import {
    IconBtn,
    IconsBox,
    Link,
    Logo,
    MainNav,
    Navbar as Nav,
    NavBarToggle,
    NavLi,
    NavigationLink,
    SearchBox,
    SearchInput,
} from './NavbarElements'

const Navbar = () => {
    const location = useLocation()

    const [displayNav, setDisplayNav] = useState(false)
    const toggleNavBar = () => {
        setDisplayNav(!displayNav)
    }

    useEffect(() => {
        displayNav ? (document.body.style.overflow = 'hidden') : (document.body.style.overflow = 'auto')
    })

    const [search, setSearch] = useState('')
    const handleOnChange = event => {
        const { value } = event.target
        setSearch(value)
    }

    return (
        <Nav>
            <NavBarToggle onClick={() => toggleNavBar()}>
                <Icon
                    name={displayNav ? 'XMark' : 'Bars-3'}
                    width='20'
                    height='20'
                    fill='var(--text-base-quaternary)'
                />
            </NavBarToggle>
            <Logo to='/'>
                Letsgo<span>Lego</span>
            </Logo>
            <Link to='' display={displayNav ? 'flex' : 'none'}>
                <Icon name='Banknotes' width='20' height='20' fill='var(--text-base-primary)' />
                Vendre
            </Link>
            <SearchBox>
                <SearchInput
                    type='text'
                    placeholder='Essayez Star Wars, Pompier...'
                    value={search}
                    onChange={handleOnChange}
                />
                <IconsBox>
                    {search && <IconBtn name='XMark' width='30' height='30' onClick={() => setSearch('')} />}
                    <IconBtn name='Magnifying' width='30' height='30' />
                </IconsBox>
            </SearchBox>
            <IconsBox>
                <NavLink to='/profil'>
                    <IconBtn
                        primary='true'
                        name={location.pathname === '/profil' ? 'UserFill' : 'User'}
                        width='34'
                        height='34'
                        color='var(--text-base-quaternary)'
                    />
                </NavLink>
                <NavLink to='/favoris'>
                    <IconBtn
                        primary='true'
                        name={location.pathname === '/favoris' ? 'HeartFill' : 'Heart'}
                        width='34'
                        height='34'
                        color='var(--text-base-quaternary)'
                    />
                </NavLink>
            </IconsBox>
            <MainNav display={displayNav ? 'flex' : 'none'}>
                <NavLi>
                    <NavigationLink>
                        Ensemble de pièces{' '}
                        <Icon name='ChevronRight' width='20' height='20' fill='var(--text-base-primary)' />
                    </NavigationLink>
                </NavLi>
                <NavLi>
                    <NavigationLink>
                        Sets classiques{' '}
                        <Icon name='ChevronRight' width='20' height='20' fill='var(--text-base-primary)' />
                    </NavigationLink>
                </NavLi>
                <NavLi>
                    <NavigationLink>
                        Sets rares <Icon name='ChevronRight' width='20' height='20' fill='var(--text-base-primary)' />
                    </NavigationLink>
                </NavLi>
                <NavLi>
                    <NavigationLink>
                        Pièces de collection{' '}
                        <Icon name='ChevronRight' width='20' height='20' fill='var(--text-base-primary)' />
                    </NavigationLink>
                </NavLi>
                <NavLi>
                    <NavigationLink>
                        Créations originales{' '}
                        <Icon name='ChevronRight' width='20' height='20' fill='var(--text-base-primary)' />
                    </NavigationLink>
                </NavLi>
            </MainNav>
        </Nav>
    )
}

export default Navbar
