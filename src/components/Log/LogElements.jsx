import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

export const Container = styled.div`
    width: 100vw;
    padding: 48px 40px 36px;

    display: flex;
    flex-direction: column;
    row-gap: 24px;
    align-items: flex-start;
    justify-content: flex-start;

    @media screen and (min-width: 475px) {
        position: relative;
        width: 448px;
        height: 500px;
        /* padding-bottom: 160px; */
        margin: 20vh auto;
        border-radius: 10px;
        border: 1px solid var(--background-primary-disabled);
    }
`

export const BackButton = styled.button`
    background: none;
    border: none;
    cursor: pointer;

    display: inline-flex;
    align-items: center;
    column-gap: 8px;

    color: var(--text-base-primary);
    font-weight: 700;
    font-size: 16px;
`

export const Title = styled.h1`
    font-family: 'Playfair Display';
    font-size: 24px;
    font-weight: 700;
    letter-spacing: -3%;
    line-height: 130%;
`

export const Form = styled.form`
    width: 100%;
    display: flex;
    flex-direction: column;
    row-gap: 24px;
    align-items: flex-start;
    justify-content: flex-start;
`

export const Button = styled.button`
    position: absolute;
    bottom: 36px;

    width: calc(100% - 40px * 2);
    border: none;
    cursor: pointer;
    padding: 16px;
    border-radius: 50px;
    background: var(--background-primary);
    color: var(--text-base-quaternary);
    font-size: 16px;
    font-weight: 700;
`

export const Link = styled(NavLink)`
    color: ${props => (props.primary ? 'var(--accent)' : 'var(--text-base-primary)')};
    font-weight: ${props => props.primary && '700'};
    text-decoration: underline;
`

export const Caption = styled.span`
    align-self: center;

    font-size: 16px;
    color: var(--text-base-secondary);
`
