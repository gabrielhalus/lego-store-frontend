import React from 'react'
import { Container, Section, SectionTitle, SectionLinks, Item, Link, Credentials } from './FooterElements'

function Footer() {
    return (
        <Container>
            <Section>
                <SectionTitle>À propos</SectionTitle>
                <SectionLinks>
                    <Item>
                        <Link to=''>Qui sommes nous ?</Link>
                    </Item>
                    <Item>
                        <Link to=''>Nous contacter</Link>
                    </Item>
                    <Item>
                        <Link to=''>Nos Guides</Link>
                    </Item>
                </SectionLinks>
            </Section>
            <Section>
                <SectionTitle>Besoin d'aide ?</SectionTitle>
                <SectionLinks>
                    <Item>
                        <Link to=''>Info consommateurs</Link>
                    </Item>
                    <Item>
                        <Link to=''>Paiement</Link>
                    </Item>
                    <Item>
                        <Link to=''>Livraison</Link>
                    </Item>
                    <Item>
                        <Link to=''>Aide et Assistance</Link>
                    </Item>
                </SectionLinks>
            </Section>
            <Section>
                <SectionTitle>La loi et l'ordre</SectionTitle>
                <SectionLinks>
                    <Item>
                        <Link to=''>Conditions générales d'utilisation</Link>
                    </Item>
                    <Item>
                        <Link to=''>Conditions générales de vente</Link>
                    </Item>
                    <Item>
                        <Link to=''>Protection des données</Link>
                    </Item>
                    <Item>
                        <Link to=''>Cookies</Link>
                    </Item>
                    <Item>
                        <Link to=''>Mentions légales</Link>
                    </Item>
                </SectionLinks>
            </Section>
            <Credentials>2022 Letsgo Lego</Credentials>
        </Container>
    )
}

export default Footer
