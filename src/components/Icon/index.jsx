import React from 'react'
import ArrowLeft from './ArrowLeft'
import Eye from './Eye'
import EyeOff from './EyeOff'
import XMark from './XMark'
import Heart from './Heart'
import HeartFill from './HeartFill'
import User from './User'
import UserFill from './UserFill'
import Magnifying from './Magnifying'
import Bars3 from './Bars-3'
import ChevronRight from './ChevronRight'
import Banknotes from './Banknotes'

const Icon = props => {
    switch (props.name.toLowerCase()) {
        case 'eye':
            return <Eye {...props} />
        case 'eyeoff':
            return <EyeOff {...props} />
        case 'arrowleft':
            return <ArrowLeft {...props} />
        case 'xmark':
            return <XMark {...props} />
        case 'heart':
            return <Heart {...props} />
        case 'heartfill':
            return <HeartFill {...props} />
        case 'user':
            return <User {...props} />
        case 'userfill':
            return <UserFill {...props} />
        case 'magnifying':
            return <Magnifying {...props} />
        case 'bars-3':
            return <Bars3 {...props} />
        case 'chevronright':
            return <ChevronRight {...props} />
        case 'banknotes':
            return <Banknotes {...props} />
        default:
            return <div />
    }
}

export default Icon
